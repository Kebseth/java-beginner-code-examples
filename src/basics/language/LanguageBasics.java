package basics.language;

// In Java, lines that start with // are called "comments". They are completely ignored when the program runs. They are very useful to the
// programmer to explain parts of the code, write documentation, etc.
/*
  This is also a comment, but this time it can span over multiple lines.
  In this project, comments are used to explain the code examples.
 */
public class LanguageBasics {

    // This is the _main method_. When a Java program starts, the code in this block is executed first.
    // You can recognize it because it is always written exactly like this.
    public static void main(String[] args) {

        // This is a _statement_. A statement is the basic building block of a java program : each statement is executed in order from top
        // to bottom.
        // This particular statement prints the string "Hello, World!" to the output of the program.
        // Please note that a statement always ends with a semicolon (;)
        System.out.println("Hello, world!");

        // This statement is a _variable declaration_. The variable with name "captainAge" is created and the value 26 is stored in the
        // variable.
        // Variables are a crucial part of most programming languages. They allow to store values to be used elsewhere in the program.
        // In java, variables are usable in the same _code block_ where they are declared (a code block starts with a `{` character and
        // ends with a `}` character)
        int captainAge = 26;

        // A variable has a _type_ (here the type is `double`) that restricts what values can be stored in the variable. There are several
        // built-in types. the most common are
        //  - `int` represents integer numbers (0, 1, 5687 or -8)
        //  - `double` represents floating point numbers like 0.7, 12.3 or -1.234
        //  - `String` stores character strings (text).
        //  - `boolean` only has two possible values : true or false.
        double chocolatePrice = 0.99;

        // On the right of the `=` is an _expression_. An expression is something that _evaluates_ to a result value. It can be a simple value (like
        // in the previous two examples) or something more complex, like variables or mathematical expressions. In this example, the value of
        // a variable is multiplied by a number. The result of this expression is assigned to a new variable.
        // The symbol `*` is called an _operator_. This ones does a mathematical multiplication.
        double chocolatePriceIncludingTaxes = chocolatePrice * 1.18;

        // this is a _condition_. Condition allow us to execute a group of statements or not based on the result of a _boolean expression_ (meaning
        // an expression with a result of type `boolean`)
        // the symbol `>` is the "greater than comparison operator". It evaluates to true or false based on the value of both operands
        if(captainAge < 50) {
          System.out.println("This is executed because the above expression evaluates to true (captainAge evaluates to 26, which is lower than 50)");
        }

        // Conditions can also have `else if` and `else` clause. only one of those is executed for a given condition.
        double availableMoney = 1;
        if(availableMoney > chocolatePriceIncludingTaxes) {
            System.out.println("I can afford this !");

            // A variable can be assigned a new value. The old value is lost !
            availableMoney = availableMoney - chocolatePriceIncludingTaxes;
        }
        else if(availableMoney == chocolatePriceIncludingTaxes) {
            System.out.println("I have just enough money, but I love chocolate too much.");
            availableMoney = 0;
        }
        else {
            System.out.println("I don't like chocolate anyway.");
        }
        System.out.println("I have " + availableMoney + " left.");

        // This is a _loop_. There are several types of loop, this one is a _while loop_. The statements inside it are executed repeatedly
        // until the _loop condition_ evaluates to false.
        double completionPercentage = 0.0;
        while (completionPercentage < 1.0) {
            System.out.println("Working on it...");
            completionPercentage = completionPercentage + 0.1;
        }
        System.out.println("Done !");

        // this is a _for loop_. It allows to write the previous loop using less lines : the initial value, loop condition and increment
        // are in a single line.
        for (double x = 0.0; x < 1; x = x + 0.1) {
            System.out.println("Working on it...");
        }
        System.out.println("Done !");
    }
}
