package basics.language;

// This is a _class_. classes are the basis of Object Oriented Programming, the main paradigm of the java language.
// Classes are two things :
//  - A way to describe composite values with several attributes
//  - The type of those values. Any variables can have a class as its type.
// A value that has a class as its type is called an _object_ or an _instance_ of this class.
// You can see a class as the "plan" or the "template" : a set of attributes and methods shared by all instances. however, the value of the
// attributes can vary from instance to instance, making each instance a separate value.
class Car {

    // this is an _attribute_ of the class. Each object will have this attribute but the value can be different for each one.
    // An attribute has a _visibility_ (private), a type (String), and a name (driverName).
    // The visibility controls whether the attribute can be accessed from outside the class (private means it can't)
    private String driverName;

    // If you specify a value for an attribute, new instances will have this value in the corresponding attribute.
    private double speed = 0;

    // This is the _constructor_ of the class. It will be executed every time a new instance is created.
    // the name must be identical to the name of the class.
    // Between the parenthesis, there are the _parameters_ of a constructor. When a new object is created, these parameters must be provided.
    public Car(String driverNameParameter) {

        // from inside the classe, the attributes can be accessed with the `this` keyword. is is not mandatory, but if you're a beginner
        // I advise you to always use it.
        // Here, the value of the `driverName` attribute is set to the value given as a parameter of the constructor.
        this.driverName = driverNameParameter;
    }

    // This is a _method_ of the class.
    // Methods are like pieces of code that can be called on an object. Inside a method, you can access all attributes and other methods of
    // the instance.
    // a method has a _visibility_ (public), a _return type_ (double), and a name (getCurrentSpeed).
    // The visibility `public` means the method can be accessed from outside the class.
    public double getCurrentSpeed() {

        // this is the _return statement_ of the method. you can put any expression or value to the right of the `return` keyword, but the
        // value or expression must have a type corresponding to the return type of the method.
        // When the method is called as part of an expression, it will evaluate to its return value.
        return this.speed;
    }

    // A method can also have _parameters_ (between the parenthesis). Parameters are value that are passed to the method when it is called.
    // They can be used inside the method like a variable.
    // `void` is a special return type meaning that the method doesn't return any value. A call to a void method can't be used inside an
    // expression.
    public void accelerate(double accelerationFactor) {
        double newSpeed = accelerationFactor * this.speed;

        // Inside a method of a class, the attributes of the current instance can be accesses or modified, but not the attributes of other
        // instances of the same classe.
        if(this.speed == 0) {
            this.speed = 1;
        }
        else {
            this.speed = newSpeed;
        }
    }
}


// This is also a class. This one is special because it contains only the main method.
public class ClassesAndObjects {

    // This is the _main method_ : it must have this signature exactly. When a
    // java program is started, this method is executed first.
    public static void main(String[] args) {
        // Here a variable is declared with the `Car` type. This variable can contain any instance of the Car class.
        Car myDadsCar;

        // An instance of a class is created with the `new` keyword. This calls the constructor of the class, passing the required parameters.
        // the `new` keyword evaluates to the new instance, and this instance is stored in the variable.
        myDadsCar = new Car("Dad");

        // With an instance, all public methods and attributes can be accessed.
        System.out.println(myDadsCar.getCurrentSpeed());

        // When calling a method of an abject, ionside the method the `this` keyword will be equal to the value that is inside the
        // `myDadsCar` variable.
        myDadsCar.accelerate(1.1);
        myDadsCar.accelerate(1.1);

        String result = "Dad is going at " + myDadsCar.getCurrentSpeed() + "km/h";
        System.out.println(result);

        // Each instance lives its life independently.
        Car myMomsCar = new Car("Mom");
        System.out.println(myMomsCar.getCurrentSpeed());
    }
}
