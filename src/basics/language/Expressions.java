package basics.language;

public class Expressions {

    public static void main(String[] args) {

        // An expression is a composition of several values. Values are composed with _operators_.
        // An expression can also contain sub-expressions, variables or method calls.
        // The expression is _evaluated_ by java to produce a _result_. The type of the result is the type of the expression.
        // Expression can not be found alone in the code (they would not be useful) : they must be part of a statement

        // the result of an expression can be assigned to a variable
        int fifteen = 3 * 5;

        // Or used as a parameter of a method
        System.out.println(fifteen + 5);

        // Here we have an expression composed of several sub-expressions. Note that the parenthesis are optional. Every operator has
        // a _precedence_ that defines if it's evaluated before or after other operators (just like in math the multiplication takes
        // precedence over the addition)
        double captainAge = (3 * 7) + Math.pow(3, 2);
        System.out.println(captainAge);

        // "Norbert" is also an expression (albeit a simple one : a single string value)
        String firstName = "James";

        // Java has all the common math operators
        int result = (4 + 5 * 6 - 4) / 2;

        // the +, when used with String operands is the _concatenation operator_. It makes a new String which is the juxtaposition of its two operands
        String fullName = firstName + " Bond";

        // The _comparison operators_ compare two values and evaluate to a boolean result. They are very useful in loops and conditions.
        boolean captainIsAdult = captainAge > 18;
        if (captainIsAdult) {
            System.out.println("Captain can drink beers !");
        }

        // java has the following comparison operators :
        // - `<` lower than
        // - `>` greater than
        // - `<=` lower than or equal to
        // - `>=` greater than or equal to
        // - `==` equal to (careful : its result can be surprising when using it with objects)

        // The boolean operators can combine boolean values according to boolean algebra rules.
        if(captainIsAdult && captainAge < 70) {
            System.out.println("This is only executed if BOTH operands of the `&&` operator are true.");
        }

        // The `!` character is the _negation operator_. It evaluates to `true` when given a false operand, and `false` if given a true
        // operand.
        if(!captainIsAdult || result == 15) {
            System.out.println("This is only executed if one of the two operands at least is true");
        }
    }
}
