package basics.language;


import java.util.*;

public class Exceptions {

    // This method can _throw an exception_. It's specified in the method signature with the `throw` keyword.
    // An exception is like a different way a method can end (the normal way being returning a value or reaching the end of the method).
    // In the method, the exception is thrown using the `throw` keyword. When a method throws an exception, the code after the statement is
    // not executed.
    static double divide(double a, double b) throws ImpossibleDivisionException {
        if (b == 0) {
            throw new ImpossibleDivisionException("Can't divide by 0 !");
        }

        return a / b;
    }


    public static void main(String[] args) {

        // When using a method that can throw an exception, the compiler will force you to either _catch_ the exception (and do something
        // about it) or throw it yourself. Since we don't want to throw this exception from the main method, we'll catch it and print an error.
        try {
            double result = divide(5.5, 0);
            System.out.println("The result is : " + result);
        } catch (ImpossibleDivisionException e) {
            System.out.println("An exception has occurred : " + e.getMessage());
        }

        // Some methods can throw exceptions, but they are not declared in the exception signature. These exceptions are called
        // _runtime exceptions_ and they behave like any other exception except they can be absent from method signatures.
        // They are usually specified in the documentation, otherwise it often means that you can't do much about them.
        // Example : Doing an invalid cast (here we try to cas a HashSet to a TreeSet) will throw an InvalidCastException, which is a
        // runtime exception.
        Set<String> studentNames = new HashSet<>();
        TreeSet<String> aSet = (TreeSet<String>) studentNames;

    }
}

// Here is the exception declaration.
// An exception is a class like any other but it extends the java.lang.Exception class.
// Exceptions have a `getMessage` method that you should override (or use a parent constructor from you exceptions's constructor)
class ImpossibleDivisionException extends Exception {

    ImpossibleDivisionException(String message) {
        super(message);
    }
}